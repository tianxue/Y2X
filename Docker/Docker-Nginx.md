1、使用国内源自动安装脚本: 
curl -fsSL https://get.docker.com | bash -s docker --mirror Aliyun

注意：安装Rancher时要注意安装版本


2、修改Docker Hub 源：
在配置文件 /etc/docker/daemon.json 中加入：
###############################################
{
  "registry-mirrors": ["https://docker.mirrors.ustc.edu.cn/"]
}
###############################################
#systemctl restart docker

3、部署案例
写Dockerfile文件：
FROM debian:jessie
MAINTAINER dugutianxue yang0054258@163.com
RUN sed -i 's/deb.debian.org/mirrors.ustc.edu.cn/g' /etc/apt/sources.list \
    && echo "Asia/Shanghai" > /etc/timezone  \
    && apt-get update \
    && apt-get install -y nginx \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* 
COPY ./index.html /usr/share/nginx/html

#ADD run.sh /run.sh
#RUN chmod 755 /run.sh

EXPOSE 80 443
CMD ["nginx", "-g", "daemon off;"]
#CMD ["/run.sh"]
